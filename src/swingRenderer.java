import core.definitions.GameState;
import core.definitions.MapObjects;
import core.definitions.Vector2;
import core.objects.BaseGameController;
import core.gameModes.*;

import javax.swing.*;
import java.awt.*;

class swingRenderer extends JPanel {
    Vector2 offset = new Vector2(50,40);
    BaseGameController gameController;
    swingRenderer(){
        gameController = new OnePlayer();
        gameController.frameHandler = aVoid -> OnFrame();
    }
    private Void OnFrame() {
        repaint();
        return null;
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        render(g);
    }
    private void DrawTexts(Graphics g){
        g.setFont(new Font("TimesRoman", Font.BOLD, 15));
        if(gameController.ScoreText!=null && gameController.ScoreText.split(":::").length>1){
            String[] scores=gameController.ScoreText.split(":::");
            g.drawString(scores[0], 8+offset.X, offset.Y-15);
            g.drawString(scores[1], this.getWidth()-160-offset.X, offset.Y-15);
        }
        else g.drawString(gameController.ScoreText, 8+offset.X, offset.Y-15);
        g.drawString("Controls: W - Up, A - Left, S - Down, D - Right", 8+offset.X, this.getHeight()-(offset.Y-25));
        if(gameController.GameState() == GameState.GAME_OVER){
            g.drawString("Press SPACE to reset", this.getWidth()/2-70, this.getHeight()/2);
            g.setFont(new Font("TimesRoman", Font.BOLD, 25));
            g.drawString("GAME OVER", this.getWidth()/2-70, this.getHeight()/2);
            g.setFont(new Font("TimesRoman", Font.BOLD, 15));
        }
        else if(gameController.GameState() == GameState.NOT_STARTED) g.drawString("Press SPACE to start", this.getWidth()/2-70, this.getHeight()/2);
    }
    private void render(Graphics g){
        Vector2 mapSize = gameController.MapSize();
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        DrawTexts(g);
        for (int y = 0; y < mapSize.Y; y++) {
            for (int x = 0; x < mapSize.X; x++){
                gameController.DrawElement(new Vector2(x, y), g);
            }
        }
    }
}
