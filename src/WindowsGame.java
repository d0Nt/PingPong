import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class WindowsGame {
    WindowsGame(){
        swingRenderer renderer = new swingRenderer();
        renderer.setSize(new Dimension(600, 600));
        //full window
        JFrame jframe = new JFrame("Ping Pong");
        jframe.setSize(585+(renderer.offset.X*2), 588+(renderer.offset.Y*2));
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jframe.add(renderer);
        jframe.setResizable(false);
        jframe.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
        jframe.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e){
                renderer.gameController.OnKeyRelease(KeyEvent.getKeyText(e.getKeyCode()).toLowerCase());
            }
            @Override
            public void keyPressed(KeyEvent e){
                renderer.gameController.OnKeyPress(KeyEvent.getKeyText(e.getKeyCode()).toLowerCase());
            }
        });
    }
}
