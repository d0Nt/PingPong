package tests

import core.definitions.Vector2
import org.junit.Test

class Vector2Test extends GroovyTestCase {
    Vector2 vectorToTest = Vector2.Zero()
    Vector2 helpVector = new Vector2(2,2)
    @Test
    void testPlus() {
        println "Testing Vector2.Plus() X"
        assertEquals(vectorToTest.X+2,vectorToTest.Plus(helpVector).X)
        println "Testing Vector2.Plus() Y"
        assertEquals(vectorToTest.Y+2,vectorToTest.Plus(helpVector).Y)
    }
    @Test
    void testMinus() {
        println "Testing vectors Vector2.Minus() X"
        assertEquals(vectorToTest.X-2,vectorToTest.Minus(helpVector).X)
        println "Testing vectors Vector2.Minus() Y"
        assertEquals(vectorToTest.Y-2,vectorToTest.Minus(helpVector).Y)
    }
}
