package core.drawing;

import core.definitions.MapObjects;
import core.definitions.Vector2;
import core.objects.Map;

import java.awt.*;

public class DrawWallStrategy implements DrawingStrategy {
    private Map map;
    private Vector2 lastPos=Vector2.Zero();
    public DrawWallStrategy(Map map){
        this.map = map;
    }
    @Override
    public boolean doesApply(Vector2 position) {
        lastPos=position;
        return map.Value(position) == MapObjects.WALL;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(lastPos.X*10+50, lastPos.Y*10+40, 10, 10);
    }
}
