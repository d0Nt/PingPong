package core.drawing;

import core.definitions.Vector2;
import core.objects.Player;

import java.awt.*;

public class DrawPlayerStrategy implements DrawingStrategy {
    private Player player;
    private Vector2 lastPos=Vector2.Zero();
    public DrawPlayerStrategy(Player player){
        this.player = player;
    }
    @Override
    public boolean doesApply(Vector2 position) {
        lastPos=position;
        return player.IsPlayer(position);
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(player.GetColor());
        g.fillRect(lastPos.X*10+50, lastPos.Y*10+40, 10, 10);
    }
}
