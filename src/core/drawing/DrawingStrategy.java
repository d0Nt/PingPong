package core.drawing;

import core.definitions.Vector2;

import java.awt.*;

public interface DrawingStrategy {
    public boolean doesApply(Vector2 position);
    public void draw(Graphics g);
}
