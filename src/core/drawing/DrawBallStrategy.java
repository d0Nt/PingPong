package core.drawing;

import core.definitions.Vector2;
import core.objects.Ball;

import java.awt.*;

public class DrawBallStrategy implements DrawingStrategy {
    private Ball ball;
    public DrawBallStrategy(Ball ball){
        this.ball = ball;
    }
    private Vector2 lastPos=Vector2.Zero();
    @Override
    public boolean doesApply(Vector2 position) {
        lastPos=position;
        return ball.IsBall(position);
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(ball.GetColor());
        g.fillOval(lastPos.X*10+50, lastPos.Y*10+40, 10, 10);
    }
}
