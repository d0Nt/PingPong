package core.commands.player;

import core.commands.CommandInterface;
import core.definitions.Direction;
import core.objects.Player;

public class MoveRight implements CommandInterface {
    private Player player;
    public MoveRight(Player player){
        this.player=player;
    }
    @Override
    public void execute() {
        this.player.Move(Direction.RIGHT);
    }
}
