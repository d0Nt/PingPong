package core.commands.player;

import core.commands.CommandInterface;
import core.definitions.Direction;
import core.objects.Player;

public class MoveUp implements CommandInterface {
    private Player player;
    public MoveUp(Player player){
        this.player=player;
    }
    @Override
    public void execute() {
        this.player.Move(Direction.UP);
    }
}
