package core.commands.player;

import core.commands.CommandInterface;
import core.definitions.Direction;
import core.objects.Player;

public class MoveLeft implements CommandInterface {
    private Player player;
    public MoveLeft(Player player){
        this.player=player;
    }
    @Override
    public void execute() {
        this.player.Move(Direction.LEFT);
    }
}
