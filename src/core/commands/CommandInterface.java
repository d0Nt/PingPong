package core.commands;

public interface CommandInterface {
    public void execute();
}
