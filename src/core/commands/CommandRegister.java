package core.commands;

import java.util.HashMap;
import java.util.Map;

public class CommandRegister {
    private Map<String, CommandInterface> commands = new HashMap<>();
    public void register(String cmd_str, CommandInterface command){
        commands.put(cmd_str, command);
    }
    public CommandInterface get(String cmd_str){
        return commands.get(cmd_str);
    }
}
