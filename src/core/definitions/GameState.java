package core.definitions;

public enum GameState {
    NOT_STARTED,
    STARTED,
    GAME_OVER
}
