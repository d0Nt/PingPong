package core.definitions;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;
    public Vector2 Vector(){
        switch (this){
            case LEFT:
                return new Vector2(-1,0);
            case RIGHT:
                return new Vector2(1,0);
            case UP:
                return new Vector2(0,-1);
            case DOWN:
                return new Vector2(0,1);
        }
        return Vector2.Zero();
    }
    public int IntValue(){
        return this.ordinal();
    }
}
