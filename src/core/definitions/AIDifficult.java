package core.definitions;

public enum AIDifficult {
    EASY,
    MEDIUM,
    HARD;
    public int IntValue(){
        return this.ordinal();
    }
}
