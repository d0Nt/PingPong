package core.definitions;

public class Vector2 {
    public int X;
    public int Y;

    public Vector2() {
    }
    public Vector2(int x, int y) {
        X = x;
        Y = y;
    }
    public Vector2 divideBy(int number){
        return new Vector2(X/number, Y/number);
    }
    public Vector2 multiplyBy(Vector2 vector){
        return new Vector2(X*vector.X, Y*vector.Y);
    }
    public boolean Between(Vector2 vect1, Vector2 vect2){
        int mX=(vect1.X+vect2.X)/2;
        int mY=(vect1.Y+vect2.Y)/2;
        return Math.abs(this.X - mX) <= Math.abs(vect1.X - mX) && Math.abs(this.Y - mY) <= Math.abs(vect1.Y - mY);
    }
    public static Vector2 Zero(){
        return new Vector2(0,0);
    }
    public Vector2 Plus(Vector2 other){
        return new Vector2(X+other.X, Y+other.Y);
    }
    public Vector2 Minus(Vector2 other){
        return new Vector2(X-other.X, Y-other.Y);
    }
    public boolean equals(Vector2 obj) {
        return (this.X == obj.X && this.Y == obj.Y);
    }
}
