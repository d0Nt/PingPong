package core.objects;
import core.definitions.Vector2;
import core.drawing.DrawBallStrategy;

public class Ball extends Entity {
    public Vector2 speed=Vector2.Zero();
    public Ball(Vector2 position) {
        this.pos = position;
        startPos = pos;
        speed = speed.Plus(new Vector2(1,1));
        draw = true;
    }
    public boolean IsBall(Vector2 check){
        return draw && check.X == pos.X && check.Y == pos.Y;
    }
    public void Bounce(boolean x){
        if(x) speed.X*=-1;
        else speed.Y*=-1;
    }
    public Vector2 NextPos(){
        return pos.Plus(speed);
    }
    public void ResetPos(){
        pos.X = startPos.X;
        pos.Y = startPos.Y;
    }
    public boolean IsMovingCloserTo(Vector2 posCheck){
        if(this.pos.X-posCheck.X<0 && speed.X>0)
            return true;
        if(this.pos.X-posCheck.X>0 && speed.X<0)
            return true;
        return false;
    }
    void Move(){
        if(!draw) return;
        pos = NextPos();
    }

    @Override
    public void OnGameControllerAdded() {
        super.OnGameControllerAdded();
        this.AddDrawingStrategy(new DrawBallStrategy(this));
    }
}
