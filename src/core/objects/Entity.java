package core.objects;

import core.definitions.Vector2;
import core.drawing.DrawingStrategy;

import java.awt.*;

abstract class Entity {
    boolean draw;
    Vector2 pos;
    Vector2 startPos;
    Color color = Color.BLACK;
    private BaseGameController gameController;
    public void AddGameController(BaseGameController controller){
        gameController = controller;
        OnGameControllerAdded();
    }
    public void SetColor(Color newColor){
        color=newColor;
    }
    public Color GetColor(){
        return color;
    }
    public void AddDrawingStrategy(DrawingStrategy strategy){
        gameController.drawingStrategies.add(strategy);
    }
    public void OnGameControllerAdded(){

    }
    public Vector2 GetPos(){
        return pos;
    }
    public Vector2 GetStartPos() {
        return startPos;
    }
}
