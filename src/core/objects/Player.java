package core.objects;
import core.commands.CommandInterface;
import core.commands.CommandRegister;
import core.definitions.AIDifficult;
import core.definitions.Direction;
import core.definitions.Vector2;
import core.drawing.DrawPlayerStrategy;

public class Player extends Entity {
    private Vector2 size;
    private String tag="Untagged";
    private Vector2[] moveBounds = new Vector2[2];
    private CommandRegister controls = new CommandRegister();
    private int score = 0;
    public int GetScore(){
        return score;
    }
    public void SetScore(int newScore){
        score=newScore;
    }
    public Player(Vector2 position, String tag) {
        pos = position;
        startPos = pos;
        moveBounds[0] = Vector2.Zero();
        moveBounds[1] = Vector2.Zero();
        draw = false;
        this.tag = tag;
    }
    public void SetSize(Vector2 size){
        this.size = size;
        draw = true;
    }
    public Vector2 GetSize(){
        return size;
    }
    //aI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private boolean aiControlled = false;
    private AIDifficult difficult = AIDifficult.EASY;
    private Vector2 hitOffset;
    public Vector2 AIHitOffset(){
        return hitOffset;
    }
    public void UpdateAIHitOffset(){
        hitOffset=new Vector2(hitOffset.X,
                (int)(Math.random()*GetSize().Y+(3-GetAIDifficult().IntValue())*2)-(GetSize().Y/2+(3-GetAIDifficult().IntValue())));
    }
    public void AddAI(boolean add, AIDifficult difficult){
        hitOffset=Vector2.Zero();
        aiControlled=add;
        this.difficult=difficult;
    }
    public boolean IsControlledByAI(){
        return aiControlled;
    }
    public AIDifficult GetAIDifficult() {
        return difficult;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public String GetTag(){
        return tag;
    }
    @Override
    public void OnGameControllerAdded() {
        this.AddDrawingStrategy(new DrawPlayerStrategy(this));
        super.OnGameControllerAdded();
    }
    //commands
    public void AddCommand(String key, CommandInterface command){
        controls.register(key, command);
    }
    public CommandInterface GetCommand(String key){
        return controls.get(key);
    }
    //moving restrictions
    public Vector2 TopLeftCorner(){
        return pos.Minus(size.divideBy(2));
    }
    public Vector2 BottomRightCorner(){
        return pos.Plus(size.divideBy(2));
    }
    public boolean IsPlayer(Vector2 posCheck) {
        return draw && posCheck.Between(TopLeftCorner(), BottomRightCorner());
    }
    public void SetBounds(Vector2 upLeft, Vector2 downRight){
        moveBounds[0] = upLeft;
        moveBounds[1] = downRight;
        if(!TopLeftCorner().Between(upLeft,downRight) || !BottomRightCorner().Between(upLeft,downRight)){
            draw = false;
            System.out.println("Player '"+tag+"' object was out of bounds.");
        }
    }
    private boolean CanMove(Direction move){
        if(!draw) return false;
        if(move == Direction.UP  && TopLeftCorner().Y<1) return false;
        if(move == Direction.LEFT  && TopLeftCorner().X<1) return false;
        if(moveBounds[0] == Vector2.Zero() && moveBounds[1] == Vector2.Zero())
            return true;
        if(move == Direction.UP && moveBounds[0].Y>=TopLeftCorner().Y)
            return false;
        if(move == Direction.LEFT && moveBounds[0].X>=TopLeftCorner().X)
            return false;
        if(move == Direction.RIGHT && moveBounds[1].X<=BottomRightCorner().X)
            return false;
        if(move == Direction.DOWN && moveBounds[1].Y<=BottomRightCorner().Y)
            return false;
        return true;
    }
    public void Move(Direction move){
        if(!CanMove(move)) return;
        this.pos = pos.Plus(move.Vector());
    }
}
