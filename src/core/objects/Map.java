package core.objects;
import core.definitions.*;
public class Map {
    private MapObjects[][] map;
    public Map(int width, int height) {
        map = new MapObjects[height][width];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                map[y][x] = MapObjects.NONE;
    }
    public void AddWalls(){
        for (int x = 0; x < Width(); x++) {
            map[0][x] = MapObjects.WALL;
            map[Height()-1][x] = MapObjects.WALL;
        }
        for (int y = 0; y < Height(); y++) {
            map[y][0] = MapObjects.WALL;
            map[y][Width()-1] = MapObjects.WALL;
        }
    }
    public MapObjects[] ObjectsAroundPoint(Vector2 nearTo){
        MapObjects[] around = new MapObjects[4];
        if(nearTo.Y-1<0) around[Direction.UP.IntValue()] = null;
        else around[Direction.UP.IntValue()]=map[nearTo.Y-1][nearTo.X];
        if(nearTo.Y+1>=Height()) around[Direction.DOWN.IntValue()] = null;
        else around[Direction.DOWN.IntValue()]=map[nearTo.Y+1][nearTo.X];
        if(nearTo.X+1>=Width()) around[Direction.RIGHT.IntValue()] = null;
        else around[Direction.RIGHT.IntValue()]=map[nearTo.Y][nearTo.X+1];
        if(nearTo.X-1<0) around[Direction.LEFT.IntValue()] = null;
        else around[Direction.LEFT.IntValue()]=map[nearTo.Y][nearTo.X-1];
        return around;
    }
    public int Height(){
        return map.length;
    }
    public int Width(){
        return map.length>0?map[0].length:0;
    }
    public MapObjects Value(Vector2 pos){
        return map[pos.Y][pos.X];
    }
}
