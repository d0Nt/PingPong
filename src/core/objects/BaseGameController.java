package core.objects;

import core.definitions.Direction;
import core.definitions.GameState;
import core.definitions.Vector2;
import core.drawing.DrawWallStrategy;
import core.drawing.DrawingStrategy;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract class BaseGameController {
    protected ArrayList<String> keysPressed = new ArrayList<>();
    public Function<Void,Void> frameHandler = null;
    private ArrayList<Player> playerList = new ArrayList<>();
    private Timer frameTimer;
    public String ScoreText="loading";
    protected Map map = null;
    protected Ball ball = null;
    protected GameState gameState;
    protected List<DrawingStrategy> drawingStrategies = new ArrayList<>();
    public GameState GameState(){
        return gameState;
    }
    protected void StartGame() {
        if (!frameTimer.isRunning()) {
            ball.ResetPos();
            frameTimer.start();
            gameState = GameState.STARTED;
        }
    }
    public void DrawElement(Vector2 pos, Graphics g){
        for (DrawingStrategy strat:drawingStrategies) {
            if(strat.doesApply(pos))
                strat.draw(g);
        }
    }
    protected void KeyControls(Player player){
        for (String key:keysPressed) {
            if(player.GetCommand(key)!=null)
                player.GetCommand(key).execute();
        }
    }
    public void OnKeyPress(String key){
        if(!keysPressed.contains(key))
            keysPressed.add(key);
    }
    public void OnKeyRelease(String key){
        if(keysPressed.contains(key))
            keysPressed.remove(key);
    }
    protected void PrepareUpdater(int updateInterval){
        frameTimer = new Timer(updateInterval, arg0 -> {
            OnGameUpdate();
            if(frameHandler != null)
                frameHandler.apply(null);
        });
        frameTimer.setRepeats(true);
    }
    protected Player GetPlayerByTag(String tag){
        for (Player player: playerList) {
            if(player.GetTag().equals(tag))
                return player;
        }
        return null;
    }
    protected void AddToList(Player player){
        playerList.add(player);

    }
    public Player IsPlayer(Vector2 vector){
        for (Player player: playerList) {
            if(player.IsPlayer(vector))
                return player;
        }
        return null;
    }
    protected void CreateMap(int width, int height){
        map = new Map(width,height);
        map.AddWalls();
        drawingStrategies.add(new DrawWallStrategy(map));
    }
    public Vector2 MapSize(){
        return new Vector2(map.Width(),map.Height());
    }
    public void OnGameUpdate(){
        for (Player plr:playerList) {
            KeyControls(plr);
            AIController(plr);
        }
        if(ball.NextPos().X>map.Width() || ball.NextPos().Y>map.Height() || ball.NextPos().X<0 || ball.NextPos().Y<0)
            ball.ResetPos();
        ColliderPhysics();
    }
    protected void OnBallCollideWithPlayer(Player player){
        if(player.IsControlledByAI()){
            player.UpdateAIHitOffset();
        }
    }
    public boolean OnBallCollideWithWall(Direction collideInWall){
        for (Player player:playerList) {
            if(player.IsControlledByAI())
                player.UpdateAIHitOffset();
        }
        return true;
    }
    private void AIController(Player player){
        if(!player.IsControlledByAI()) return;
        if(ball.IsMovingCloserTo(player.GetPos())){
            if(player.GetPos().Y+player.AIHitOffset().Y>ball.GetPos().Y) {
                player.Move(Direction.UP);
            }
            else if(player.GetPos().Y+player.AIHitOffset().Y<ball.GetPos().Y)
                player.Move(Direction.DOWN);
        }else{
            if(player.GetPos().Y>player.GetStartPos().Y) {
                player.Move(Direction.UP);
            }
            else if(player.GetPos().Y<player.GetStartPos().Y)
                player.Move(Direction.DOWN);
        }
    }
    private void ColliderPhysics() {
        if(ball==null)
            return;
        if(ball.NextPos().Y>map.Height()-2 || ball.NextPos().Y<1){
            if(ball.NextPos().Y<1)
                OnBallCollideWithWall(Direction.UP);
            else
                OnBallCollideWithWall(Direction.DOWN);
            ball.Bounce(false);
        }
        if(ball.NextPos().X>map.Width()-2 || ball.NextPos().X<1){
            if(ball.NextPos().X<1)
                OnBallCollideWithWall(Direction.LEFT);
            else
                OnBallCollideWithWall(Direction.RIGHT);
            ball.Bounce(true);
        }
        Player playerCollision=IsPlayer(ball.NextPos());
        if(playerCollision!=null){
            ball.Bounce(true);
            OnBallCollideWithPlayer(playerCollision);
        }
        ball.Move();
    }
}
