package core.gameModes;
import core.commands.player.*;
import core.definitions.*;
import core.objects.Ball;
import core.objects.BaseGameController;
import core.objects.Player;

public class TwoPlayers extends BaseGameController {
    public TwoPlayers() {
        CreateMap(58, 56);
        AddPlayer();
        AddBall();
        gameState = GameState.NOT_STARTED;
        this.PrepareUpdater(40);
    }
    private void AddPlayer(){
        Player player = new Player(new Vector2(3,map.Height()/2), "Player");
        player.SetSize(new Vector2(2,10));
        player.SetBounds(new Vector2(1,1),new Vector2(map.Width()/2-10,map.Height()-1));
        player.AddGameController(this);
        this.AddToList(player);
        player.AddCommand("w", new MoveUp(player));
        player.AddCommand("s", new MoveDown(player));
        player.AddCommand("d", new MoveRight(player));
        player.AddCommand("a", new MoveLeft(player));

        Player enemy = new Player(new Vector2(map.Width()-5,map.Height()/2), "Enemy");
        enemy.SetSize(new Vector2(2,10));
        enemy.SetBounds(new Vector2(map.Width()/2+10,1),new Vector2(map.Width()-1,map.Height()-1));
        enemy.AddGameController(this);
        this.AddToList(enemy);
        enemy.AddCommand("numpad-8", new MoveUp(enemy));
        enemy.AddCommand("numpad-2", new MoveDown(enemy));
        enemy.AddCommand("numpad-6", new MoveRight(enemy));
        enemy.AddCommand("numpad-4", new MoveLeft(enemy));
        ScoreText="Score: "+player.GetScore()+":::Score: "+enemy.GetScore();
    }
    private void AddBall(){
        if(map == null){
            System.out.println("First add map");
            return;
        }
        ball = new Ball(new Vector2(map.Width()/2,map.Height()/2));
        ball.AddGameController(this);
    }

    @Override
    public boolean OnBallCollideWithWall(Direction collideInWall) {
        System.out.println(collideInWall);
        Player player = GetPlayerByTag("Player");
        Player enemy = GetPlayerByTag("Enemy");
        if(collideInWall == Direction.LEFT){
            if(enemy!=null) {
                enemy.SetScore(enemy.GetScore() + 1);
                ScoreText = "Score: " + player.GetScore() + ":::Score: " + enemy.GetScore();
            }
        }
        if(collideInWall == Direction.RIGHT){
            if(player!=null) {
                player.SetScore(player.GetScore() + 1);
                ScoreText = "Score: " + player.GetScore() + ":::Score: " + enemy.GetScore();
            }
        }
        return super.OnBallCollideWithWall(collideInWall);
    }
    @Override
    public void OnKeyRelease(String key) {
        if(keysPressed.contains("space")) {
            this.StartGame();
        }
        super.OnKeyRelease(key);
    }
}
