package core.gameModes;

import core.commands.player.*;
import core.definitions.AIDifficult;
import core.definitions.Direction;
import core.definitions.GameState;
import core.definitions.Vector2;
import core.objects.Ball;

import core.objects.BaseGameController;
import core.objects.Player;

import java.awt.*;

public class OnePlayer extends BaseGameController {
    public OnePlayer() {
        CreateMap(58,56);
        AddPlayer();
        AddBall();
        gameState = GameState.NOT_STARTED;
        this.PrepareUpdater(30);
    }
    private void AddPlayer(){
        Player player = new Player(new Vector2(2,map.Height()/2), "Player");
        player.SetSize(new Vector2(2,10));
        player.SetBounds(new Vector2(1,1),new Vector2(map.Width()/2-10,map.Height()-2));
        player.AddGameController(this);
        player.AddCommand("w", new MoveUp(player));
        player.AddCommand("s", new MoveDown(player));
        player.AddCommand("d", new MoveRight(player));
        player.AddCommand("a", new MoveLeft(player));
        player.SetColor(Color.BLUE);
        AddToList(player);

        Player enemy = new Player(new Vector2(map.Width()-3,map.Height()/2), "Enemy");
        enemy.SetSize(new Vector2(2,10));
        enemy.SetBounds(new Vector2(map.Width()/2+10,1),new Vector2(map.Width()-1,map.Height()-2));
        enemy.AddGameController(this);
        enemy.AddAI(true, AIDifficult.EASY);
        enemy.SetColor(Color.BLUE);
        AddToList(enemy);
        //players score
        ScoreText="Score: "+player.GetScore()+":::Score: "+enemy.GetScore();
    }
    private void AddBall(){
        if(map == null){
            System.out.println("First add map");
            return;
        }
        ball = new Ball(new Vector2(map.Width()/2,map.Height()/2));
        ball.AddGameController(this);
        ball.SetColor(Color.CYAN);
    }

    @Override
    public boolean OnBallCollideWithWall(Direction collideInWall) {
        Player player = GetPlayerByTag("Player");
        Player enemy = GetPlayerByTag("Enemy");
        if(collideInWall == Direction.LEFT){
            if(enemy!=null) {
                enemy.SetScore(enemy.GetScore() + 1);
                ScoreText = "Score: " + player.GetScore() + ":::Score: " + enemy.GetScore();
            }
        }
        if(collideInWall == Direction.RIGHT){
            if(player!=null) {
                player.SetScore(player.GetScore() + 1);
                ScoreText = "Score: " + player.GetScore() + ":::Score: " + enemy.GetScore();
            }
        }
        return super.OnBallCollideWithWall(collideInWall);
    }
    @Override
    public void OnKeyRelease(String key) {
        if(keysPressed.contains("space")) {
            this.StartGame();
        }
        super.OnKeyRelease(key);
    }
}
